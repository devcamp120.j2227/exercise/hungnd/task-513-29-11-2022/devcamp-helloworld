import gDevcampReact from "./info";

function App() {
  /*
  function countPercentStudyingStudent() {
    return gDevcampReact.studyingStudents / gDevcampReact.totalStudents * 100;
  } 
  var sPercentStudyingStudent = countPercentStudyingStudent();
  */
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src= {gDevcampReact.image} alt="Devcamp" width={500}></img>
      <p>Tỷ lệ học sinh đang theo học là: {gDevcampReact.countPercentStudyingStudent()}</p>
      <p> {gDevcampReact.countPercentStudyingStudent() > 15 ? "Sinh viên đăng ký học nhiều" : "Sinh viên đăng ký học ít"}</p>
      <ul>
        {gDevcampReact.benefits.map((item, index) => {
          return <li>{item}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
